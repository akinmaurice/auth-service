import express from 'express';


import expressConfig from './config/express';

const port = process.env.PORT || 3070;
const app = express();

expressConfig(app);

app.listen(port);
logger.info(`Auth Service started on port ${port}`);

export default app;
