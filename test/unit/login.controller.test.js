import chai from 'chai';
import sinon from 'sinon';
import rewire from 'rewire';
import bcrypt from 'bcrypt';

import db from '../../app/utils/database';


const loginUser = rewire('../../app/controllers/login/login.controller.js');

const should = chai.should();
const { expect } = chai;
let sandbox;


describe('It validates all Functions for forgot password', () => {
    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });
    it('Validation check should fail for request body', async() => {
        const request = {
        };
        const checkRequest = loginUser.__get__('checkRequest');
        await expect(checkRequest(request)).to.be.rejected;
    });


    it('Validation check should pass', async() => {
        const request = {
            email: 'akin@gmail.com',
            password: '12345'
        };
        const checkRequest = loginUser.__get__('checkRequest');
        const response = await checkRequest(request);
        response.should.equal(true);
    });


    it('Should fail to find user', async() => {
        const email = 'akin@gmail.com';
        sandbox.stub(db, 'oneOrNone').returns(Promise.resolve());
        const getUser = loginUser.__get__('getUser');
        await expect(getUser(email)).to.be.rejected;
    });


    it('Should find user but return error as user is not activated', async() => {
        const email = 'akin@gmail.com';
        const user = {
            id: 'user-12345',
            name: 'Akin',
            email: 'akin@ya.com',
            is_verified: false
        };
        sandbox.stub(db, 'oneOrNone').returns(Promise.resolve(user));
        const getUser = loginUser.__get__('getUser');
        await expect(getUser(email)).to.be.rejected;
    });


    it('Should find user', async() => {
        const email = 'akin@gmail.com';
        const user = {
            id: 'user-12345',
            name: 'Akin',
            email: 'akin@ya.com',
            is_verified: true
        };
        sandbox.stub(db, 'oneOrNone').returns(Promise.resolve(user));
        const getUser = loginUser.__get__('getUser');
        const response = await getUser(email);
        response.should.equal(user);
    });


    it('Should fail to verify User Password', async() => {
        const password = 'Akinben';
        const user = {
            id: 'user-12345',
            name: 'Akin',
            email: 'akin@ya.com',
            is_verified: true,
            salt: 'Hmm',
            hash: '$2b$10$tgdjdjd'
        };
        const verifyPassword = loginUser.__get__('verifyPassword');
        await expect(verifyPassword(password, user)).to.be.rejected;
    });


    it('Should verify user Password', async() => {
        const password = 'Akinben';
        const user = {
            id: 'user-12345',
            name: 'Akin',
            email: 'akin@ya.com',
            is_verified: true,
            salt: '$2b$10$tg3VgeHS81F1A00v9gB7xO',
            hash: '$2b$10$tg3VgeHS81F1A00v9gB7xOCVMf/kZ0ybSOMAfjQUTeXR1hIjF9sTy'
        };
        const verifyPassword = loginUser.__get__('verifyPassword');
        const response = await verifyPassword(password, user);
        response.should.equal(user);
    });


    it('Should modify user details', async() => {
        const user = {
            id: 'user-12345',
            name: 'Akin',
            email: 'akin@ya.com',
            is_verified: true,
            salt: '$2b$10$tg3VgeHS81F1A00v9gB7xO',
            hash: '$2b$10$tg3VgeHS81F1A00v9gB7xOCVMf/kZ0ybSOMAfjQUTeXR1hIjF9sTy'
        };
        const modifyUserDetails = loginUser.__get__('modifyUserDetails');
        const response = await modifyUserDetails(user);
        response.should.have.property('id');
        response.should.have.property('first_name');
        response.should.have.property('role');
    });

    it('Should create user token', async() => {
        const user = {
            id: 'user-12345',
            first_name: 'Akin',
            role: 'C'
        };
        const createUserToken = loginUser.__get__('createUserToken');
        const response = await createUserToken(user);
        response.should.be.an('object');
    });
});
