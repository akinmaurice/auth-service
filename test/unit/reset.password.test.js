import chai from 'chai';
import sinon from 'sinon';
import moment from 'moment';
import rewire from 'rewire';

import db from '../../app/utils/database';


const resetPassword = rewire('../../app/controllers/reset-password/reset-password.js');

const should = chai.should();
const { expect } = chai;
let sandbox;


describe('It validates all Functions to reset password', () => {
    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });
    it('Validation check should fail for request body', async() => {
        const request = {
        };
        const checkRequest = resetPassword.__get__('checkRequest');
        await expect(checkRequest(request)).to.be.rejected;
    });


    it('Validation check should fail passwords do not match', async() => {
        const request = {
            verification_code: 'hdhdhdh',
            password: '12345',
            confirmPassword: '12'
        };
        const checkRequest = resetPassword.__get__('checkRequest');
        await expect(checkRequest(request)).to.be.rejected;
    });


    it('Validation check should pass', async() => {
        const request = {
            verification_code: 'hdhdhdh',
            password: '12345',
            confirmPassword: '12345'
        };
        const checkRequest = resetPassword.__get__('checkRequest');
        const response = await checkRequest(request);
        response.should.equal(true);
    });


    it('Should fail to find user', async() => {
        const verification_code = '12344';
        sandbox.stub(db, 'oneOrNone').returns(Promise.resolve());
        const getUser = resetPassword.__get__('getUser');
        await expect(getUser(verification_code)).to.be.rejected;
    });


    it('Should find user but return error as user is not activated', async() => {
        const verification_code = '12344';
        const user = {
            id: 'user-12345',
            name: 'Akin',
            email: 'akin@ya.com',
            is_verified: false
        };
        sandbox.stub(db, 'oneOrNone').returns(Promise.resolve(user));
        const getUser = resetPassword.__get__('getUser');
        await expect(getUser(verification_code)).to.be.rejected;
    });


    it('Should find user', async() => {
        const verification_code = '12344';
        const user = {
            id: 'user-12345',
            name: 'Akin',
            email: 'akin@ya.com',
            is_verified: true
        };
        sandbox.stub(db, 'oneOrNone').returns(Promise.resolve(user));
        const getUser = resetPassword.__get__('getUser');
        const response = await getUser(verification_code);
        response.should.equal(user);
    });


    it('Should fail as token has expired', async() => {
        const user = {
            id: 'user-12345',
            name: 'Akin',
            email: 'akin@ya.com',
            is_verified: true,
            updated_at: '2018-01-01'
        };
        sandbox.stub(db, 'oneOrNone').returns(Promise.resolve(user));
        const checkExpiryTime = resetPassword.__get__('checkExpiryTime');
        await expect(checkExpiryTime(user)).to.be.rejected;
    });


    it('Should pass as token is still valid', async() => {
        const user = {
            id: 'user-12345',
            name: 'Akin',
            email: 'akin@ya.com',
            is_verified: true,
            updated_at: moment()
        };
        sandbox.stub(db, 'oneOrNone').returns(Promise.resolve(user));
        const checkExpiryTime = resetPassword.__get__('checkExpiryTime');
        const response = await checkExpiryTime(user);
        response.should.equal(true);
    });


    it('Should generate salt and hash for user password', async() => {
        const request = {
            verification_code: 'hdhdhdh',
            password: '12345',
            confirmPassword: '12345'
        };
        const processPassword = resetPassword.__get__('processPassword');
        const response = await processPassword(request);
        response.should.have.property('hash');
        response.should.have.property('salt');
    });


    it('Should update user password', async() => {
        const passwordData = {
            hash: 'hshjjhs',
            salt: 'jsjdkdkhdk'
        };
        const user_id = 'user-12345';
        sandbox.stub(db, 'none').returns(Promise.resolve());
        const updateUserPassword = resetPassword.__get__('updateUserPassword');
        const response = await updateUserPassword(passwordData, user_id);
        response.should.equal(true);
    });
});
