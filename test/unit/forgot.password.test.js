import chai from 'chai';
import sinon from 'sinon';
import rewire from 'rewire';

import db from '../../app/utils/database';


const forgotPassword = rewire('../../app/controllers/forgot-password/forgot-password.js');

const should = chai.should();
const { expect } = chai;
let sandbox;


describe('It validates all Functions for forgot password', () => {
    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });
    it('Validation check should fail for request body', async() => {
        const request = {
        };
        const checkRequest = forgotPassword.__get__('checkRequest');
        await expect(checkRequest(request)).to.be.rejected;
    });


    it('Validation check should pass', async() => {
        const request = {
            email: 'akin@gmail.com'
        };
        const checkRequest = forgotPassword.__get__('checkRequest');
        const response = await checkRequest(request);
        response.should.equal(true);
    });


    it('Should fail to find user', async() => {
        const email = 'akin@gmail.com';
        sandbox.stub(db, 'oneOrNone').returns(Promise.resolve());
        const getUser = forgotPassword.__get__('getUser');
        await expect(getUser(email)).to.be.rejected;
    });


    it('Should find user', async() => {
        const email = 'akin@gmail.com';
        const user = {
            id: 'user-12345',
            name: 'Akin',
            email: 'akin@ya.com'
        };
        sandbox.stub(db, 'oneOrNone').returns(Promise.resolve(user));
        const getUser = forgotPassword.__get__('getUser');
        const response = await getUser(email);
        response.should.equal(user);
    });

    it('Should generate token to reset password ', async() => {
        const user = {
            id: 'user-12345',
            name: 'Akin',
            email: 'akin@ya.com'
        };
        sandbox.stub(db, 'none').returns(Promise.resolve());
        const generateToken = forgotPassword.__get__('generateToken');
        const response = await generateToken(user.id);
        response.should.be.a('string');
    });
});
