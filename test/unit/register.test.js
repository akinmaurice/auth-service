import chai from 'chai';
import sinon from 'sinon';
import rewire from 'rewire';

import db from '../../app/utils/database';


const registerUser = rewire('../../app/controllers/register/register.controller.js');

const should = chai.should();
const { expect } = chai;
let sandbox;


describe('It validates all Functions to create a user', () => {
    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });
    it('Validation check should fail for request body', async() => {
        const request = {
        };
        const checkRequest = registerUser.__get__('checkRequest');
        await expect(checkRequest(request)).to.be.rejected;
    });


    it('Validation check fail as passwords do not match', async() => {
        const request = {
            email: 'akin@gmail.com',
            password: '12345',
            confirmPassword: '125',
            phone_number: '123848484',
            first_name: 'Akin',
            last_name: 'Ben'
        };
        const checkRequest = registerUser.__get__('checkRequest');
        await expect(checkRequest(request)).to.be.rejected;
    });


    it('Validation check should pass', async() => {
        const request = {
            email: 'akin@gmail.com',
            password: '12345',
            confirmPassword: '12345',
            phone_number: '123848484',
            first_name: 'Akin',
            last_name: 'Ben'
        };
        const checkRequest = registerUser.__get__('checkRequest');
        const response = await checkRequest(request);
        response.should.equal(true);
    });


    it('Verification for email and phone should fail as email/phone exists', async() => {
        const request = {
            email: 'akin@gmail.com',
            password: '12345',
            confirmPassword: '12345',
            phone_number: '123848484',
            first_name: 'Akin',
            last_name: 'Ben'
        };
        const result = {
            count: 1
        };
        sandbox.stub(db, 'oneOrNone').returns(Promise.resolve(result));
        const verifyUserEmailAndPhone = registerUser.__get__('verifyUserEmailAndPhone');
        await expect(verifyUserEmailAndPhone(request)).to.be.rejected;
    });


    it('Verification for email and phone should pass', async() => {
        const request = {
            email: 'akin@gmail.com',
            password: '12345',
            confirmPassword: '12345',
            phone_number: '123848484',
            first_name: 'Akin',
            last_name: 'Ben'
        };
        const result = {
            count: 0
        };
        sandbox.stub(db, 'oneOrNone').returns(Promise.resolve(result));
        const verifyUserEmailAndPhone = registerUser.__get__('verifyUserEmailAndPhone');
        const response = await verifyUserEmailAndPhone(request);
        response.should.equal(true);
    });


    it('Should Hash Password', async() => {
        const request = {
            email: 'akin@gmail.com',
            password: '12345',
            confirmPassword: '12345',
            phone_number: '123848484',
            first_name: 'Akin',
            last_name: 'Ben'
        };
        const hashUserPassword = registerUser.__get__('hashUserPassword ');
        const response = await hashUserPassword(request.password);
        response.should.have.property('salt');
        response.should.have.property('hash');
    });


    it('Should save user address', async() => {
        const request = {
            email: 'akin@gmail.com',
            password: '12345',
            confirmPassword: '12345',
            phone_number: '123848484',
            first_name: 'Akin',
            last_name: 'Ben'
        };
        const address = {
            id: 'add-12345'
        };
        sandbox.stub(db, 'one').returns(Promise.resolve(address));
        const saveUserAddress = registerUser.__get__('saveUserAddress');
        const response = await saveUserAddress(request);
        response.should.equal(address);
    });


    it('Should create user', async() => {
        const request = {
            email: 'akin@gmail.com',
            password: '12345',
            confirmPassword: '12345',
            phone_number: '123848484',
            first_name: 'Akin',
            last_name: 'Ben'
        };
        const address = {
            id: 'user-12345'
        };
        const passwordData = {
            salt: 'salt33983893',
            hash: 'jfjkfkjfkfkh'
        };
        const user = {
            id: 'user-12345'
        };
        sandbox.stub(db, 'one').returns(Promise.resolve(user));
        const createUser = registerUser.__get__('createUser');
        const response = await createUser(request, address, passwordData);
        response.should.have.property('user');
        response.should.have.property('verification_code');
        response.user.should.equal(user);
    });


    it('Should create user role', async() => {
        const request = {
            email: 'akin@gmail.com',
            password: '12345',
            confirmPassword: '12345',
            phone_number: '123848484',
            first_name: 'Akin',
            last_name: 'Ben'
        };
        const user = {
            id: 'user-12345'
        };
        sandbox.stub(db, 'none').returns(Promise.resolve());
        const createUserRole = registerUser.__get__('createUserRole');
        const response = await createUserRole(request, user);
        response.should.equal(true);
    });
});
