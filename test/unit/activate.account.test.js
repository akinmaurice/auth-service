import chai from 'chai';
import sinon from 'sinon';
import rewire from 'rewire';

import db from '../../app/utils/database';


const activateUser = rewire('../../app/controllers/activate/activate.account.js');

const should = chai.should();
const { expect } = chai;
let sandbox;


describe('It validates all Functions to activate an account', () => {
    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });
    it('Validation check should fail for request body', async() => {
        const request = {
        };
        const checkRequest = activateUser.__get__('checkRequest');
        await expect(checkRequest(request)).to.be.rejected;
    });


    it('Validation check should pass', async() => {
        const request = {
            verification_code: 'verify-j3h3hhj3hb'
        };
        const checkRequest = activateUser.__get__('checkRequest');
        const response = await checkRequest(request);
        response.should.equal(true);
    });


    it('Should fail to find user', async() => {
        const verification_code = 'verify-j3h3hhj3hb';
        sandbox.stub(db, 'oneOrNone').returns(Promise.resolve());
        const getUser = activateUser.__get__('getUser');
        await expect(getUser(verification_code)).to.be.rejected;
    });


    it('Should find user', async() => {
        const verification_code = 'verify-j3h3hhj3hb';
        const user = {
            id: 'user-12345',
            name: 'Akin',
            email: 'akin@ya.com',
            is_verified: false
        };
        sandbox.stub(db, 'oneOrNone').returns(Promise.resolve(user));
        const getUser = activateUser.__get__('getUser');
        const response = await getUser(verification_code);
        response.should.equal(user);
    });
});
