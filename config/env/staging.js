const staging = {
    AUTH_SERVICE_DATABASE_URL: process.env.AUTH_SERVICE_STAGING_DATABASE_URL,
    RABBIT_MQ_URL: process.env.RABBIT_MQ_STAGING_URL
};

export default staging;
