const production = {
    AUTH_SERVICE_DATABASE_URL: process.env.AUTH_SERVICE_PROD_DATABASE_URL,
    RABBIT_MQ_URL: process.env.RABBIT_MQ_PROD_URL
};

export default production;
