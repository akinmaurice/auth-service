const development = {
    AUTH_SERVICE_DATABASE_URL: process.env.AUTH_SERVICE_DEV_DATABASE_URL,
    RABBIT_MQ_URL: process.env.RABBIT_MQ_DEV_URL
};

export default development;
