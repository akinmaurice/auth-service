const test = {
    AUTH_SERVICE_DATABASE_URL: process.env.AUTH_SERVICE_TEST_DATABASE_URL,
    RABBIT_MQ_URL: process.env.RABBIT_MQ_TEST_URL
};

export default test;
