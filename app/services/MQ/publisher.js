import amqp from 'amqplib/callback_api';
import config from '../../../config';


const messagePublisher = (queue, payload) => {
    amqp.connect(config.RABBIT_MQ_URL, (err, conn) => {
        conn.createChannel((err, ch) => {
            ch.assertQueue(queue, { durable: true });
            ch.sendToQueue(queue, Buffer.from(JSON.stringify(payload)), {
                persistent: true
            });
            logger.info(`Message sent to Queue ${queue}`);
        });
    });
};


export default messagePublisher;
