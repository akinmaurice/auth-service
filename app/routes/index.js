import express from 'express';
import loginUser from '../controllers/login/login.controller';
import forgotPassword from '../controllers/forgot-password/forgot-password';
import activateAccount from '../controllers/activate/activate.account';
import resetPassword from '../controllers/reset-password/reset-password';
import registerUser from '../controllers/register/register.controller';

const router = express.Router();

router.get('/', (req, res) => {
    res.status(200).json({ message: 'Auth Service' });
});


router.post(
    '/user/register',
    registerUser
);


router.post(
    '/user/login',
    loginUser
);


router.post(
    '/user/forgot-password',
    forgotPassword
);


router.post(
    '/user/activate',
    activateAccount
);


router.post(
    '/user/reset-password',
    resetPassword
);


export default router;
