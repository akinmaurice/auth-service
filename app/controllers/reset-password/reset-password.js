import Q from 'q';
import moment from 'moment';
import bcrypt from 'bcrypt';
import checkRequestBody from '../../utils/request.body.verifier';
import Response from '../../utils/response';
import config from '../../../config';
import db from '../../utils/database';
import query from '../../queries';

const ResponseHandler = new Response();


const checkRequest = (body) => {
    const defer = Q.defer();
    const error = checkRequestBody(body, [
        'verification_code',
        'password',
        'confirmPassword'
    ]);
    if (error) {
        defer.reject({
            code: 400,
            msg: error
        });
    } else if (body.password !== body.confirmPassword) {
        defer.reject({
            code: 400,
            msg: 'Passwords do not match'
        });
    } else {
        defer.resolve(true);
    }
    return defer.promise;
};


const getUser = async(verification_code) => {
    const defer = Q.defer();
    try {
        const user = await db.oneOrNone(query.getUserByVerificationCode, [ verification_code ]);
        if (!user) {
            defer.reject({
                code: 400,
                msg: 'could not find that user'
            });
        } else if (!user.is_verified) {
            defer.reject({
                code: 400,
                msg: 'Invalid Error'
            });
        } else {
            defer.resolve(user);
        }
    } catch (e) {
        logger.error('Reset-Pass-User-Get-User-Error', e, {
            serviceName: config.serviceName
        });
        defer.reject({
            code: 500,
            msg: 'Unknown Error'
        });
    }
    return defer.promise;
};


const checkExpiryTime = (user) => {
    const defer = Q.defer();
    const current_time = moment();
    const created_time = moment(user.updated_at);
    const expiryTime = (config.accountActivationExpiryTime * 60 * 60);
    const timeDiff = (current_time.diff(created_time, 'seconds'));
    if (timeDiff > expiryTime) {
        defer.reject({
            code: 400,
            msg: 'Reset Password token has expired'
        });
    } else {
        defer.resolve(true);
    }
    return defer.promise;
};


const processPassword = (body) => {
    const defer = Q.defer();
    const { password } = body;
    const saltRounds = 10;
    bcrypt.genSalt(saltRounds, (e, salt) => {
        bcrypt.hash(password, salt, (err, hash) => {
            if (err) {
                logger.error('Hash-User-Password-Error', err, {
                    serviceName: config.serviceName
                });
                defer.reject({
                    code: 500,
                    msg: 'Unknown Error'
                });
            } else {
                const data = {
                    hash,
                    salt
                };
                defer.resolve(data);
            }
        });
    });
    return defer.promise;
};


const updateUserPassword = async(passwordData, user_id) => {
    const defer = Q.defer();
    const { salt, hash } = passwordData;
    const updated_at = moment();
    try {
        const promise = Q.all([
            db.none(query.resetPassword, [ salt, hash, updated_at, user_id ]),
            db.none(query.removeVerificationCode, [ null, user_id ])
        ]);
        await promise;
        defer.resolve(true);
    } catch (e) {
        logger.error('Reset-Pass-Update-User-Error', e, {
            serviceName: config.serviceName
        });
        defer.reject({
            code: 500,
            msg: 'Unknown Error'
        });
    }
    return defer.promise;
};


async function resetPassword(req, res) {
    const { body } = req;
    const { verification_code } = body;
    try {
        await checkRequest(body);
        const user = await getUser(verification_code);
        const { id } = user;
        await checkExpiryTime(user);
        const passwordData = await processPassword(body);
        await updateUserPassword(passwordData, id);
        res.status(200).json(ResponseHandler.success(req.originalUrl, {
            message: 'User account password updated'
        }));
    } catch (e) {
        res.status(e.code).json(ResponseHandler.error(
            req.originalUrl,
            e.msg,
            'Bad Request'
        ));
    }
}


export default resetPassword;
