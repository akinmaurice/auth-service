import Q from 'q';
import moment from 'moment';
import crypto from 'crypto';
import bcrypt from 'bcrypt';
import checkRequestBody from '../../utils/request.body.verifier';
import Response from '../../utils/response';
import db from '../../utils/database';
import query from '../../queries';
import config from '../../../config';
import messagePublisher from '../../services/MQ/publisher';


const ResponseHandler = new Response();


const checkRequest = (body) => {
    const defer = Q.defer();
    const error = checkRequestBody(body, [
        'email',
        'phone_number',
        'first_name',
        'last_name',
        'password',
        'confirmPassword'
    ]);
    if (error) {
        defer.reject({
            code: 400,
            msg: error
        });
    } else if (body.password !== body.confirmPassword) {
        defer.reject({
            code: 400,
            msg: 'Passwords do not match'
        });
    } else {
        defer.resolve(true);
    }

    return defer.promise;
};


const verifyUserEmailAndPhone = async(body) => {
    const defer = Q.defer();
    const { email, phone_number } = body;
    try {
        const promise = Q.all([
            db.oneOrNone(query.verifyEmail, [ email ]),
            db.oneOrNone(query.verifyPhoneNumber, [ phone_number ])
        ]);
        const result = await promise;
        const emailCount = result[0].count;
        const phoneCount = result[1].count;
        if (emailCount === 0 && phoneCount === 0) {
            defer.resolve(true);
        } else {
            defer.reject({
                code: 400,
                msg: 'A user with that email or phone number exists already'
            });
        }
    } catch (e) {
        logger.error('Verify-User-Email/Phone-Error', e, {
            serviceName: config.serviceName
        });
        defer.reject({
            code: 400,
            msg: 'Unknown Error'
        });
    }
    return defer.promise;
};


const hashUserPassword = (password) => {
    const defer = Q.defer();
    const saltRounds = 10;
    bcrypt.genSalt(saltRounds, (e, salt) => {
        bcrypt.hash(password, salt, (err, hash) => {
            if (err) {
                logger.error('Hash-User-Password-Error', err, {
                    serviceName: config.serviceName
                });
                defer.reject({
                    code: 500,
                    msg: 'Unknown Error'
                });
            } else {
                const passwordData = {
                    salt,
                    hash
                };
                defer.resolve(passwordData);
            }
        });
    });
    return defer.promise;
};


const saveUserAddress = async(body) => {
    const defer = Q.defer();
    const city = body.city || null;
    const country_code = body.country_code || null;
    const created_at = moment();
    const updated_at = moment();
    try {
        const address = await db.one(
            query.createUserAddress,
            [ city, country_code, created_at, updated_at ]
        );
        defer.resolve(address);
    } catch (e) {
        logger.error('Save-User-Address-Error', e, {
            serviceName: config.serviceName
        });
        defer.reject({
            code: 500,
            msg: 'Unknown Error'
        });
    }
    return defer.promise;
};


const createUser = async(body, address, passwordData) => {
    const defer = Q.defer();
    const created_at = moment();
    const updated_at = moment();
    const is_active = true;
    const is_verified = false;
    const {
        email, phone_number, first_name, last_name
    } = body;
    const verification_code = crypto.randomBytes(20).toString('hex');
    const { id } = address;
    const { salt, hash } = passwordData;
    try {
        const user = await db.one(query.registerUser, [ email, phone_number, first_name,
            last_name, salt, hash, id, is_verified, is_active,
            verification_code, created_at, updated_at ]);
        const data = {
            user,
            verification_code
        };
        defer.resolve(data);
    } catch (e) {
        logger.error('Create-User-Error', e, {
            serviceName: config.serviceName
        });
        defer.reject({
            code: 500,
            msg: 'Unknown Error'
        });
    }
    return defer.promise;
};


const createUserRole = async(body, user) => {
    const defer = Q.defer();
    const created_at = moment();
    const updated_at = moment();
    const role_code = body.role_code || 'C';
    const user_id = user.id;
    try {
        await db.none(query.createUserRole, [ user_id, role_code, created_at, updated_at ]);
        defer.resolve(true);
    } catch (e) {
        logger.error('Create-User-Role-Error', e, {
            serviceName: config.serviceName
        });
        defer.reject({
            code: 400,
            msg: 'Unknown Error'
        });
    }
    return defer.promise;
};


const sendRegistrationEmail = (body, user, verification_code) => {
    const defer = Q.defer();
    const data = {
        email: body.email,
        id: user.id,
        first_name: body.first_name,
        last_name: body.last_name,
        name: body.first_name,
        verification_code
    };
    messagePublisher(config.messageQueue.userRegistration, data);
    messagePublisher(config.messageQueue.onBoardUser, data);
    defer.resolve('Registration Successful');
    return defer.promise;
};


async function registerUser(req, res) {
    const { body } = req;
    const { password } = body;
    try {
        await checkRequest(body);
        await verifyUserEmailAndPhone(body);
        const passwordData = await hashUserPassword(password);
        const address = await saveUserAddress(body);
        const data = await createUser(body, address, passwordData);
        const { user, verification_code } = data;
        await createUserRole(body, user);
        const response = await sendRegistrationEmail(body, user, verification_code);
        res.status(200).json(ResponseHandler.success(req.originalUrl, {
            message: 'Successfully registered user account',
            data: response
        }));
    } catch (e) {
        res.status(e.code).json(ResponseHandler.error(
            req.originalUrl,
            e.msg,
            'Bad Request'
        ));
    }
}


export default registerUser;
