import Q from 'q';
import moment from 'moment';
import crypto from 'crypto';
import checkRequestBody from '../../utils/request.body.verifier';
import Response from '../../utils/response';
import config from '../../../config';
import db from '../../utils/database';
import query from '../../queries';

const ResponseHandler = new Response();


const checkRequest = (body) => {
    const defer = Q.defer();
    const error = checkRequestBody(body, [
        'email'
    ]);
    if (error) {
        defer.reject({
            code: 400,
            msg: error
        });
    }
    defer.resolve(true);
    return defer.promise;
};


const getUser = async(email) => {
    const defer = Q.defer();
    try {
        const user = await db.oneOrNone(query.getUserByEmail, [ email ]);
        if (!user) {
            defer.reject({
                code: 400,
                msg: 'Could not find that user'
            });
        } else {
            defer.resolve(user);
        }
    } catch (e) {
        logger.error('Get-User-Reset-Password-Error', e, {
            serviceName: config.serviceName
        });
        defer.reject({
            code: 500,
            msg: 'Unknown Error'
        });
    }
    return defer.promise;
};


const generateToken = async(user_id) => {
    const defer = Q.defer();
    const updated_at = moment();
    const verification_code = crypto.randomBytes(20).toString('hex');
    try {
        await db.none(
            query.forgotPassword,
            [
                verification_code,
                updated_at,
                user_id
            ]
        );
        defer.resolve(verification_code);
    } catch (e) {
        logger.error('Update-User-Forgot-Pass-Error', e, {
            serviceName: config.serviceName
        });
        defer.reject({
            code: 500,
            msg: 'Unknown Error'
        });
    }
    return defer.promise;
};


    // Publish MQ to send forgotPass mail

async function forgotPassword(req, res) {
    try {
        const { body } = req;
        const { email } = body;
        await checkRequest(body);
        const user = await getUser(email);
        const { id } = user;
        const verification_code = await generateToken(id);
        res.status(200).json(ResponseHandler.success(req.originalUrl, {
            message: 'Password Reset email sent',
            verification_code
        }));
    } catch (e) {
        res.status(e.code).json(ResponseHandler.error(
            req.originalUrl,
            e.msg,
            'Bad Request'
        ));
    }
}


export default forgotPassword;
