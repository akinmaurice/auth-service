import Q from 'q';
import bcrypt from 'bcrypt';


import checkRequestBody from '../../utils/request.body.verifier';
import Response from '../../utils/response';
import config from '../../../config';
import db from '../../utils/database';
import query from '../../queries';
import generateToken from '../../utils/auth/generate.token';


const ResponseHandler = new Response();


const checkRequest = (body) => {
    const defer = Q.defer();
    const error = checkRequestBody(body, [
        'email',
        'password'
    ]);
    if (error) {
        defer.reject({
            code: 400,
            msg: error
        });
    }
    defer.resolve(true);
    return defer.promise;
};


const getUser = async(email) => {
    const defer = Q.defer();
    try {
        const user = await db.oneOrNone(query.loginUser, [ email ]);
        if (!user) {
            defer.reject({
                code: 400,
                msg: 'Wrong Email/Password'
            });
        } else if (!user.is_verified) {
            defer.reject({
                code: 400,
                msg: 'Please activate your account to sign in'
            });
        } else {
            defer.resolve(user);
        }
    } catch (e) {
        logger.error('Get-User--Error', e, {
            serviceName: config.serviceName
        });
        defer.reject({
            code: 500,
            msg: 'Unknown Error'
        });
    }
    return defer.promise;
};


const verifyPassword = (password, user) => {
    const defer = Q.defer();
    const { hash, salt } = user;
    bcrypt.hash(password, salt, (err, passwordHash) => {
        if (err) {
            logger.error('Verify-Password-Error', err, {
                serviceName: config.serviceName
            });
            defer.reject({
                code: 400,
                msg: 'Unknown Error'
            });
        } else if (passwordHash !== hash) {
            defer.reject({
                code: 400,
                msg: 'Invalid Email/Password'
            });
        } else {
            defer.resolve(user);
        }
    });
    return defer.promise;
};


const modifyUserDetails = (user) => {
    const defer = Q.defer();
    const newUser = {
        id: user.id,
        first_name: user.first_name,
        role: user.role_code
    };
    defer.resolve(newUser);
    return defer.promise;
};


const createUserToken = async(user) => {
    const defer = Q.defer();
    try {
        const token = await generateToken(user);
        const data = {
            token,
            user
        };
        defer.resolve(data);
    } catch (e) {
        logger.error('Generate-Token-Error', e, {
            serviceName: config.serviceName
        });
        defer.reject({
            code: 500,
            msg: 'Unknown Error'
        });
    }
    return defer.promise;
};


async function loginUser(req, res) {
    try {
        const { body } = req;
        const { email, password } = body;
        await checkRequest(body);
        const user = await getUser(email);
        await verifyPassword(password, user);
        const updatedUser = await modifyUserDetails(user);
        const response = await createUserToken(updatedUser);
        res.status(200).json(ResponseHandler.success(req.originalUrl, {
            message: 'User Logged in',
            login_data: response
        }));
    } catch (e) {
        res.status(e.code).json(ResponseHandler.error(
            req.originalUrl,
            e.msg,
            'Bad Request'
        ));
    }
}


export default loginUser;
