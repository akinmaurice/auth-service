import moment from 'moment';
import Q from 'q';
import checkRequestBody from '../../utils/request.body.verifier';
import Response from '../../utils/response';
import config from '../../../config';
import db from '../../utils/database';
import query from '../../queries';


const ResponseHandler = new Response();


const checkRequest = (body) => {
    const defer = Q.defer();
    const error = checkRequestBody(body, [
        'verification_code'
    ]);
    if (error) {
        defer.reject({
            code: 400,
            msg: error
        });
    }
    defer.resolve(true);
    return defer.promise;
};


const getUser = async(verification_code) => {
    const defer = Q.defer();
    try {
        const user = await db.oneOrNone(query.getUserByVerificationCode, [ verification_code ]);
        if (!user) {
            defer.reject({
                code: 404,
                msg: 'Invalid Verification code'
            });
        } else if (user.is_verified) {
            defer.reject({
                code: 400,
                msg: 'Invalid Error'
            });
        } else {
            defer.resolve(user);
        }
    } catch (e) {
        logger.error('Activate-User-Get-User-Error', e, {
            serviceName: config.serviceName
        });
        defer.reject({
            code: 500,
            msg: 'Unknown Error'
        });
    }
    return defer.promise;
};


const checkExpiryTime = (user) => {
    const defer = Q.defer();
    const current_time = moment();
    const created_time = moment(user.created_at);
    const expiryTime = (config.accountActivationExpiryTime * 60 * 60);
    const timeDiff = (current_time.diff(created_time, 'seconds'));
    if (timeDiff > expiryTime) {
        defer.reject({
            code: 400,
            msg: 'Activation token has expired'
        });
    }
    defer.resolve(true);
    return defer.promise;
};


const activateAccount = async(user_id) => {
    const defer = Q.defer();
    try {
        const promise = Q.all([
            db.none(query.activateAccount, [ true, user_id ]),
            db.none(query.removeVerificationCode, [ null, user_id ])
        ]);
        await promise;
        defer.resolve(true);
    } catch (e) {
        logger.error('Activate-User-Update-Error', e, {
            serviceName: config.serviceName
        });
        defer.reject('Unknown Error');
    }
    return defer.promise;
};


async function activateUser(req, res) {
    const { body } = req;
    const { verification_code } = body;
    try {
        await checkRequest(body);
        const user = await getUser(verification_code);
        const { id } = user;
        await checkExpiryTime(user);
        await activateAccount(id);
        res.status(200).json(ResponseHandler.success(req.originalUrl, {
            message: 'User account Verified'
        }));
    } catch (e) {
        res.status(e.code).json(ResponseHandler.error(
            req.originalUrl,
            e.msg,
            'Bad Request'
        ));
    }
}


export default activateUser;
