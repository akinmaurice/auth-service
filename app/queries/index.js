const queries = {
    loginUser: `
        SELECT
            users.id,
            users.first_name,
            users.salt,
            users.hash,
            users.created_at,
            users.is_verified,
            user_roles.role_code
        FROM
            users
        LEFT JOIN
            user_roles ON user_roles.user_id = users.id
        WHERE
            email = $1
    `,
    getUserByVerificationCode: `
        SELECT
            id,
            created_at,
            updated_at,
            is_verified,
            email
        FROM
            users
        WHERE
            verification_code = $1
    `,
    activateAccount: `
        UPDATE
            users
        SET
            is_verified = $1
        WHERE
            id = $2
    `,
    getUserByEmail: `
        SELECT
            id
        FROM
            users
        WHERE
            email = $1
    `,
    forgotPassword: `
        UPDATE
            users
        SET
            verification_code = $1,
            updated_at = $2
        WHERE
            id = $3
    `,
    resetPassword: `
        UPDATE
            users
        SET
            salt = $1,
            hash = $2,
            updated_at = $3
        WHERE
            id = $4
    `,
    registerUser: `
        INSERT INTO
            users(
                email,
                phone_number,
                first_name,
                last_name,
                salt,
                hash,
                address_id,
                is_verified,
                is_active,
                verification_code,
                created_at,
                updated_at
            )
        VALUES(
                $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12
            ) RETURNING id
    `,
    createUserAddress: `
        INSERT INTO
            user_address(
                city,
                country_code,
                created_at,
                updated_at
            )
        VALUES(
                $1, $2, $3, $4
            ) RETURNING id
    `,
    createUserRole: `
        INSERT INTO
            user_roles(
                user_id,
                role_code,
                created_at,
                updated_at
            )
        VALUES(
            $1, $2, $3, $4
        )
    `,
    verifyEmail: `
        SELECT
            count(*)::float
        FROM
            users
        WHERE
            email = $1
    `,
    verifyPhoneNumber: `
        SELECT
            count(*)::float
        FROM
            users
        WHERE
            phone_number = $1
    `,
    removeVerificationCode: `
        UPDATE
            users
        SET
            verification_code = NULL
        WHERE
            id = $2
    `
};

export default queries;
