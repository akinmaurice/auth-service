import jwt from 'jsonwebtoken';
import Q from 'q';
import fs from 'fs';
import config from '../../../config';


const privateKey = fs.readFileSync('./private.key', 'utf8');


const signOptions = {
    issuer: config.auth.issuer,
    subject: config.auth.subject,
    audience: config.auth.audience,
    expiresIn: config.auth.expiresIn,
    algorithm: 'RS256'
};

const generateToken = (user) => {
    const defer = Q.defer();
    const token = jwt.sign(user, privateKey, signOptions);
    defer.resolve(token);
    return defer.promise;
};

export default generateToken;
