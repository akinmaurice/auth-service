FROM node:10.14.2-alpine


# Create app directory
RUN mkdir -p /usr/src/auth-service
WORKDIR /usr/src/auth-service

COPY package.json /usr/src/auth-service/

RUN apk update && apk upgrade \
	&& apk add --no-cache git \
	&& apk --no-cache add --virtual builds-deps build-base python \
	&& npm install -g nodemon gulp node-gyp node-pre-gyp && npm install\
	&& npm rebuild bcrypt --build-from-source

# Bundle app source
COPY . /usr/src/auth-service

EXPOSE 3070

CMD ["npm", "start"]

